int api_openwin(char *buf, int xsiz, int ysiz, int col_inv, char *title);
void api_putstrwin(int win, int x, int y, int col, int len, char *str);
void api_boxfilwin(int win, int x0, int y0, int x1, int y1, int col);
void api_initmalloc(void);
char *api_malloc(int size);
void api_refreshwin(int win, int x0, int y0, int x1, int y1);
void api_linewin(int win, int x0, int y0, int x1, int y1, int col);
void api_closewin(int win);
int api_getkey(int mode);
void api_end(void);
int rand(void);

#define sx 160
#define sy 120
#define wtitle 32
#define wx sx
#define wy sy + wtitle
#define psx sx / 16 / 5
#define psy sy / 12
#define psp sy / 20
#define bs psx

void HariMain(void)
{
	char *buf;
	int win, i, j = 0, p1x, p1y, p2x, p2y, bx, by, bvx, bvy;
	api_initmalloc();
	buf = api_malloc(wx * wy);
	win = api_openwin(buf, wx, wy, -1, "Pong");
	api_boxfilwin(win, 0, 33, wx - 1, wy - 1, 0 /*  */);
	p1x = sx / 16;
	p1y = sy / 2 + wtitle;
	p2x = sx - sx / 16;
	p2y = sy / 2 + wtitle;
	bx = sx / 2;
	by = sy / 2 + wtitle;
	bvx = sx / 50;
	bvy = 0;

	api_boxfilwin(win, p1x - psx, p1y - psy, p1x + psx, p1y + psy, 1 /* Τ */);
	api_boxfilwin(win, p2x - psx, p2y - psy, p2x + psx, p2y + psy, 6 /* Β */);
	api_boxfilwin(win, bx - bs, by - bs, bx + bs, by + bs, 7 /*  */);

	for (;;) {
		i = api_getkey(1);
		if (i == 0x0a) { break; } /* EnterΕIΉ */

		if (i == 'r') {
			api_boxfilwin(win, p1x - psx, p1y - psy, p1x + psx, p1y + psy, 0 /*  */);
			p1y -= psp;
			if (p1y < wtitle + psy + 1) { p1y = wtitle + psy + 1; }
			api_boxfilwin(win, p1x - psx, p1y - psy, p1x + psx, p1y + psy, 1 /* Τ */);
		}
		if (i == 'f') {
			api_boxfilwin(win, p1x - psx, p1y - psy, p1x + psx, p1y + psy, 0 /*  */);
			p1y += psp;
			if (p1y > sy + wtitle - psy - 1) { p1y = sy + wtitle - psy - 1; }
			api_boxfilwin(win, p1x - psx, p1y - psy, p1x + psx, p1y + psy, 1 /* Τ */);
		}
		if (i == 'u') {
			api_boxfilwin(win, p2x - psx, p2y - psy, p2x + psx, p2y + psy, 0 /*  */);
			p2y -= psp;
			if (p2y < wtitle + psy + 1) { p2y = wtitle + psy + 1; }
			api_boxfilwin(win, p2x - psx, p2y - psy, p2x + psx, p2y + psy, 6 /* Β */);
		}
		if (i == 'j') {
			api_boxfilwin(win, p2x - psx, p2y - psy, p2x + psx, p2y + psy, 0 /*  */);
			p2y += psp;
			if (p2y > sy + wtitle - psy - 1) { p2y = sy + wtitle - psy - 1; }
			api_boxfilwin(win, p2x - psx, p2y - psy, p2x + psx, p2y + psy, 6 /* Β */);
		}

		if (j == 300000) {
			api_boxfilwin(win, bx - bs, by - bs, bx + bs, by + bs, 0 /*  */);
			bx += bvx;
			by += bvy;

			if (by < wtitle + bs + 1) {
				by = wtitle + bs + 1;
				bvy = -bvy;
			}

			if (by > sy + wtitle - bs - 1) {
				by = sy + wtitle - bs - 1;
				bvy = -bvy;
			}

			if (p1x - psx <= bx && bx <= p1x + psx &&
				p1y - psy <= by && by <= p1y + psy)
			{
				bvx = -bvx;
				bvy = (by - p1y) / 2;
			}

			if (p2x - psx <= bx && bx <= p2x + psx &&
				p2y - psy <= by && by <= p2y + psy)
			{
				bvx = -bvx;
				bvy = (by - p2y) / 2;
			}

			if (-50 > bx || bx > sx + 50) {
				bx = sx / 2;
				by = sy / 2 + wtitle;
			}
			
			if (0 < bx && bx < sx) {
				api_boxfilwin(win, p1x - psx, p1y - psy, p1x + psx, p1y + psy, 1 /* Τ */);
				api_boxfilwin(win, p2x - psx, p2y - psy, p2x + psx, p2y + psy, 6 /* Β */);

				api_boxfilwin(win, bx - bs, by - bs, bx + bs, by + bs, 7 /*  */);
			}

			j = 0;
		}

		j++;
	}
	api_closewin(win);
	api_end();
}
