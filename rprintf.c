/* return print format�֐� */

#include "bootpack.h"
#include <stdio.h>
#include <stdarg.h>

char *rprintf(char *format, ...)
{
	va_list ap;
	char s[1000];

	va_start(ap, format);
	vsprintf(s, format, ap);
	va_end(ap);
	return s;
}
