/* ウィンドウ関係 */

#include "bootpack.h"

void make_window8(unsigned char *buf, int xsize, int ysize, char *title, char act)
{
	boxfill8(buf, xsize, COL8_FFFFFF, 0, 33, xsize - 1, ysize - 1);
	//boxfill8(buf, xsize, bc, 0, ysize - 1, 0, ysize - 1);
	//boxfill8(buf, xsize, bc, xsize - 1, ysize - 1, xsize - 1, ysize - 1);
	make_wtitle8(buf, xsize, title, act);
	return;
}

void make_wtitle8(unsigned char *buf, int xsize, char *title, char act)
{
	static char fullbtn[14][16] = {
		"................",
		"..OOOO....OOOO..",
		"..OOOO....OOOO..",
		"..OO........OO..",
		"..OO........OO..",
		"................",
		"................",
		"................",
		"................",
		"..OO........OO..",
		"..OO........OO..",
		"..OOOO....OOOO..",
		"..OOOO....OOOO..",
		"................"
	};
	static char closebtn[14][16] = {
		"................",
		"..OO........OO..",
		"..OOO......OOO..",
		"...OOO....OOO...",
		"....OOO..OOO....",
		".....OOOOOO.....",
		"......OOOO......",
		"......OOOO......",
		".....OOOOOO.....",
		"....OOO..OOO....",
		"...OOO....OOO...",
		"..OOO......OOO..",
		"..OO........OO..",
		"................"
	};
	int x, y;
	char c, tc, tbc;
	if (act != 0) {
		tc = COL8_000000;
		tbc = COL8_C6C6C6;
	} else {
		tc = COL8_000000;
		tbc = COL8_848484;
	}
	boxfill8(buf, xsize, tbc, 0, 0, xsize - 1, 32);
	//boxfill8(buf, xsize, bc, 0, 0, 0, 0);
	//boxfill8(buf, xsize, bc, xsize - 1, 0, xsize - 1, 0);
	putfonts8_asc(buf, xsize, 32, 8, tc, title);
	for (y = 0; y < 14; y++) {
		for (x = 0; x < 16; x++) {
			c = closebtn[y][x];
			if (c == 'O') {
				c = tc;
			} else {
				c = tbc;
			}
			buf[(9 + y) * xsize + (xsize - 24 + x)] = c;
		}
	}
	for (y = 0; y < 14; y++) {
		for (x = 0; x < 16; x++) {
			c = fullbtn[y][x];
			if (c == 'O') {
				c = tc;
			} else {
				c = tbc;
			}
			buf[(9 + y) * xsize + (xsize - 48 + x)] = c;
		}
	}
	return;
}

void putfonts8_asc_sht(struct SHEET *sht, int x, int y, int c, int b, char *s, int l)
{
	boxfill8(sht->buf, sht->bxsize, b, x, y, x + l * 8 - 1, y + 15);
	putfonts8_asc(sht->buf, sht->bxsize, x, y, c, s);
	sheet_refresh(sht, x, y, x + l * 8, y + 16);
	return;
}

void make_textbox8(struct SHEET *sht, int x0, int y0, int sx, int sy, int c)
{
	int x1 = x0 + sx, y1 = y0 + sy;
	boxfill8(sht->buf, sht->bxsize, COL8_848484, x0 - 2, y1 + 1, x1 + 1, y1 + 1);
	boxfill8(sht->buf, sht->bxsize, c, x0 - 1, y0 - 1, x1 + 0, y1 + 0);
	return;
}

void change_wtitle8(struct SHEET *sht, char act)
{
	int x, y, xsize = sht->bxsize;
	char c, tc_new, tbc_new, tc_old, tbc_old, *buf = sht->buf;
	if (act != 0) {
		tc_new = COL8_000000;
		tbc_new = COL8_C6C6C6;
		tc_old = COL8_000000;
		tbc_old = COL8_848484;
	} else {
		tc_new = COL8_000000;
		tbc_new = COL8_848484;
		tc_old = COL8_000000;
		tbc_old = COL8_C6C6C6;
	}
	for (y = 0; y < 33; y++) {
		for (x = 0; x < xsize; x++) {
			c = buf[y * xsize + x];
			if (c == tc_old && x <= xsize - 22) {
				c = tc_new;
			} else if (c == tbc_old) {
				c = tbc_new;
			}
			buf[y * xsize + x] = c;
		}
	}
	sheet_refresh(sht, 0, 0, xsize, 33);
	return;
}

void make_taskber8(unsigned char *buf, int xsize, int ysize)
{
	static char startbtn[32][32] = {
		"................................",
		"..............OOOO..............",
		"............OOOOOOOO............",
		"...........OOOOOOOOOO...........",
		"..........OOOOOOOOOOOO..........",
		"..........OOOOOOOOOOOO..........",
		".........OOOOOOOOOOOOOO.........",
		".........OOOOOOOOOOOOOO.........",
		".........OOOOOOOOOOOOOO.........",
		".........OOOOOOOOOOOOOO.........",
		"..........OOOOOOOOOOOO..........",
		"..........OOOOOOOOOOOO..........",
		"...........OOOOOOOOOO...........",
		"............OOOOOOOO............",
		"..............OOOO..............",
		"................................",
		"................................",
		"................................",
		"................................",
		"..............OOOO..............",
		".........OOOOOOOOOOOOOO.........",
		"......OOOOOOOOOOOOOOOOOOOO......",
		"....OOOOOOOOOOOOOOOOOOOOOOOO....",
		"...OOOOOOOOOOOOOOOOOOOOOOOOOO...",
		"..OOOOOOOOOOOOOOOOOOOOOOOOOOOO..",
		".OOOOOOOOOOOOOOOOOOOOOOOOOOOOOO.",
		".OOOOOOOOOOOOOOOOOOOOOOOOOOOOOO.",
		".OOOOOOOOOOOOOOOOOOOOOOOOOOOOOO.",
		".OOOOOOOOOOOOOOOOOOOOOOOOOOOOOO.",
		".OOOOOOOOOOOOOOOOOOOOOOOOOOOOOO.",
		"................................",
		"................................",
	};
	int x, y;
	char c;
	boxfill8(buf, xsize, COL8_000000, 0, 0, xsize - 1, ysize - 1);
	for (y = 0; y < 32; y++) {
		for (x = 0; x < 32; x++) {
			c = startbtn[y][x];
			if (c == 'O') {
				c = COL8_FFFFFF;
			} else {
				c = COL8_000000;
			}
			buf[(8 + y) * xsize + (8 + x)] = c;
		}
	}
	return;
}
